#!/bin/sh
set -e

docker daemon \
    --host=unix:///var/run/docker.sock \
    --host=tcp://0.0.0.0:2375 \
    $DOCKER_DAEMON_FLAGS \
    > /etc/docker-daemon-logs 2>&1 &

echo "waiting for docker daemon..."

while ! (docker ps) >/dev/null 2>&1;
    do sleep 1;
done;


docker version
docker-compose version

# workaruond for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/987
keep_alive() {
  while sleep 30s; do
    echo .
  done
}

keep_alive &

exec "$@"
