#!/bin/sh
set -o xtrace

# echo '*' > /tmp/docker-gc-exclude-containers
# docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/docker-gc-exclude-containers:/etc/docker-gc-exclude-containers spotify/docker-gc

# stop and remove all containers running for over an hour
docker rm -fv $(docker ps -a | grep -e hours -e days -e months -e years | grep dind | sed 's/ .*//g')

docker volume rm $(docker volume ls -f dangling=true -q) || true
(docker network ls | grep dind | sed 's/ .*//g' | xargs -n 1 docker network rm) || true

# remove all built images older than a week
docker rmi $(docker images | grep -P '(((\d\d)|[6-9])[ ]days)|(weeks)|(months)' | grep '^dind' | sed 's/ .*//g')
docker rmi $(docker images -f dangling=True -q) || true
