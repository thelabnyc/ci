#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o xtrace

docker-compose up -d db
docker-compose run web
