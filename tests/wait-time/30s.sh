#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o xtrace

echo 'First output'

sleep 30s

echo 'Second output'
