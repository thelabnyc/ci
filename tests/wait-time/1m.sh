#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o xtrace

echo 'First output'

sleep 1m

echo 'Second output'
